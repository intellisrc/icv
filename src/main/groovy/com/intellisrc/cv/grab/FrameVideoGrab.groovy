package com.intellisrc.cv.grab

import com.intellisrc.core.Log
import com.intellisrc.core.SysClock
import com.intellisrc.cv.Converter
import com.intellisrc.cv.VideoGrab
import com.intellisrc.img.FrameShot
import groovy.transform.CompileStatic
import org.bytedeco.javacv.FFmpegFrameGrabber
import org.bytedeco.javacv.Frame
import static org.bytedeco.ffmpeg.global.avutil.*

import java.awt.image.BufferedImage

/**
 * Grab images from a video file
 *
 * FrameGrabber will try:
 "DC1394", "FlyCapture", "FlyCapture2", "OpenKinect", "PS3Eye", "VideoInput", "OpenCV", "FFmpeg", "IPCamera"
 *
 * @since 2020/02/19.
 */
@CompileStatic
class FrameVideoGrab extends VideoGrab {
    protected int width = 0
    protected int height = 0

    static {
        av_log_set_level(AV_LOG_ERROR)
    }

    interface FrameFramer {
        boolean call(Frame img)
    }

    /**
     * Initialize Frame Grab with source
     * @param source
     */
    FrameVideoGrab(String source = "") {
        if(source) {
            this.source = source
        }
    }

    /**
     * Set video size (not always needed)
     * @param width
     * @param height
     */
    void setVideoSize(int width, int height) {
        this.width = width
        this.height = height
    }

    /**
     * Return a FrameShot instead of Frame
     * @param frameCallback
     * @param finishCallback
     */
    @Override
    void grabFrameShot(FrameShotCallback frameCallback, GrabFinished onFinish = null) {
        grab((FrameFramer) {
            Frame frame ->
                boolean ok = false
                if(frame) {
                    try {
                        BufferedImage bufferedImage = Converter.FrameToBuffered(frame)
                        String frameName = "frame-" + SysClock.dateTime.format(timeFormat)
                        FrameShot frameShot = new FrameShot(bufferedImage, frameName)
                        ok = frameCallback.call(frameShot)
                    } catch(Exception e) {
                        Log.w("Unable to return frame: %s (%s)", e.message, e.cause)
                    }
                } else {
                    Log.w("Frame was empty")
                }
                return ok
        }, onFinish)
    }

    /**
     * Read all frames from source and call frameCallback for each of them
     * and finishCallback at the end
     * @param frameCallback
     * @param finishCallback
     */
    void grab(FrameFramer frameCallback, GrabFinished onFinish = null) {
        Log.v("Reading encoded video: %s", source)
        boolean next = true
        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(source)
        //grabber.setPixelFormat(AV_PIX_FMT_YUVJ422P)
        if(width) { grabber.imageWidth = width }
        if(height) { grabber.imageHeight = height }
        grabber.setFormat(format ?: source.split(/\./).last())
        grabber.start()

        while (next) {
            Frame frame = grabber.grab()
            if(frame) {
                if (frame.imageWidth) {
                    next = frameCallback.call(frame)
                }
            } else {
                next = false
            }
        }
        if(onFinish) {
            onFinish.call()
        }
    }
}
