# ICV Library
[![GPL Licence](https://badges.frapsoft.com/os/gpl/gpl.svg?v=103)](https://opensource.org/licenses/GPL-3.0/)
[![Open Source Love](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![Maven Repository](https://maven-badges.herokuapp.com/maven-central/com.intellisrc/core/badge.svg?color=blue)](https://mvnrepository.com/artifact/com.intellisrc/)

## Goal

Make it easier to work with JavaCV / OpenCV (Computer Vision Library)

**NOTE**: This project used to be part of [ICL](https://gitlab.com/intellisrc/common) library.

## Usage
How to use it in your project:

Maven:
```xml
<dependency>
  <groupId>com.intellisrc</groupId>
  <artifactId>cv</artifactId>
  <version>VERSION</version>
</dependency>
```
Gradle:
```groovy
dependencies {
    implementation 'com.intellisrc:cv:VERSION'
    // or extended annotation:
    implementation group: 'com.intellisrc', name: 'cv', version: 'VERSION'
}
```

Follow the instructions on the last published version for each module in [maven repository](https://mvnrepository.com/artifact/com.intellisrc/)

In which `VERSION` is for example: `2.8.x`.

## Overview

**Notes**: 
* classes marked with `★` are the most commonly used classes for that module. 
* classes marked with `@`, are unlikely to be used directly, but may be of your interest. 

### [cv](modules/cv/README.md)

> Classes for Computer Vision (extension to OpenCV). Convert image formats, crop, rotate images
> or draw objects on top of them. It simplifies grabbing images from any video source.
> [read more...](modules/cv/README.md)

|     | Class               | Usage                                                                 |
|-----|---------------------|-----------------------------------------------------------------------|
 | ★   | `Converter`         | Convert from and to CV image formats (extends img.Converter)          |
 | ★   | `CvTools`           | Perform common operations in images : rotate, resize, etc             |
 | ★   | `FrameShot`         | Extends img.FrameShot to support CV image format                      |
 | @   | `JpegFormat`        | JPEG Format                                                           |
 | @   | `MjpegFormat`       | MJPEG Format                                                          |
 | @   | `MjpegFrame`        | MJPEG single frame                                                    |
 | @   | `MjpegInputStream`  | Provides MJPEG as InputStream                                         |
 | @   | `VideoGrab`         | Extract frames from most video formats                                |
 |     | `BufferedVideoGrab` | Get BufferedImages or FrameShot/CvFrameShot from a MJPEG stream       |
 |     | `FileVideoGrab`     | Get File or FrameShot/CvFrameShot images from a directory (as frames) |
 | ★   | `FrameVideoGrab`    | Get Frame or FrameShot/CvFrameShot objects from video files           |

[JavaDoc](https://intellisrc.gitlab.io/common/#cv)
